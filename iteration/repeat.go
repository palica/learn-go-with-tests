package iteration

// Repeat takes a character and number of repetitions
// and outputs the character that number of times
func Repeat(character string, repeatCount int) string {
	var repeated string
	for x := 0; x < repeatCount; x++ {
		repeated += character
	}
	return repeated
}
