package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {

	assertCorrectResult := func(t *testing.T, got, want string) {
		if got != want {
			t.Errorf("got %q wanted %q", got, want)
		}
	}

	t.Run("testing positive integer", func(t *testing.T) {
		got := Repeat("a", 5)
		want := "aaaaa"
		assertCorrectResult(t, got, want)
	})

	t.Run("testing negative integer", func(t *testing.T) {
		got := Repeat("a", -4)
		want := ""
		assertCorrectResult(t, got, want)
	})
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

func ExampleRepeat() {
	chars := Repeat("a", 10)
	fmt.Println(chars)
	// Output: aaaaaaaaaa
}
