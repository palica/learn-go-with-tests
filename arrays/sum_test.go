package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {

	assertCorrectResult := func(t *testing.T, got, want int) {
		t.Helper()
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	}

	t.Run("testing positive numbers", func(t *testing.T) {
		numbers := []int{3, 4, 5, 6, 7}
		got := Sum(numbers)
		want := 25
		assertCorrectResult(t, got, want)
	})

}

func TestSumAll(t *testing.T) {

	got := SumAll([]int{1, 2}, []int{0, 9})
	want := []int{3, 9}

	// Go does not let you use equality operators with slices.
	// You could write a function to iterate over each got and
	// want slice and check their values but for convenience sake,
	// we can use reflect.DeepEqual which is useful for seeing
	// if any two variables are the same.Interesting!
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestSumAllTails(t *testing.T) {

	t.Run("sums of some slices", func(t *testing.T) {
		got := SumAllTails([]int{1, 2}, []int{0, 9})
		want := []int{2, 9}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}
	})

	t.Run("sum of empty slices", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{3, 4, 5})
		want := []int{0, 9}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}
	})

	t.Run("sum of slices with one item", func(t *testing.T) {
		got := SumAllTails([]int{1}, []int{3})
		want := []int{0, 0}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}
	})
}

func ExampleSumAllTails() {
	sum := SumAllTails([]int{1, 2, 3}, []int{-1, 2, 3})
	fmt.Println(sum)
	// Output: [5 5]

}
