package main

// Sum takes array of numbers and returns the sum of them
func Sum(numbers []int) int {
	var sum int
	for _, x := range numbers {
		sum += x
	}
	return sum
}

// SumAll takes any number of slices with integers and returns sums
// of the individual slices in a slice (Talking like TemptorSent)<---lol
func SumAll(numbersToSum ...[]int) (sums []int) {
	// the book tells you to write minimal code that is working
	// slice of slices?
	//var sums []int
	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers))
	}
	return
}

// SumAllTails takes slices again, but skips first item from each slice
func SumAllTails(numbersToSum ...[]int) (sums []int) {
	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers[1:]))
	}
	return
}

func main() {
}
